﻿using System.Threading;

namespace Diarco.Exp.PedidosConfirmados
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var mutexId = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            using (var mutex = new Mutex(false, mutexId))
            {
                if (!mutex.WaitOne(0, false))
                {
                    return;
                }
                //AutoMapperConfiguration.Configure();
                new Job().Execute(args);
            }
        }
    }
}
