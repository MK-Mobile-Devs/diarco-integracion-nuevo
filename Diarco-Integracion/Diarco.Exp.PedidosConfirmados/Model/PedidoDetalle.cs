﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Diarco.Exp.PedidosConfirmados.Model
{
    public class PedidoDetalle
    {
        [XmlRoot(ElementName = "PostD")]
        public class PostD
        {
            [XmlElement(ElementName = "C_DOC")]
            public string C_DOC { get; set; }
            [XmlElement(ElementName = "U_DOC_PREFIJO")]
            public string U_DOC_PREFIJO { get; set; }
            [XmlElement(ElementName = "U_DOC_SUFIJO")]
            public string U_DOC_SUFIJO { get; set; }
            [XmlElement(ElementName = "C_ARTICULO")]
            public string C_ARTICULO { get; set; }
            [XmlElement(ElementName = "N_ARTICULO")]
            public string N_ARTICULO { get; set; }
            [XmlElement(ElementName = "U_ITEM_PEDIDO")]
            public string U_ITEM_PEDIDO { get; set; }
            [XmlElement(ElementName = "I_PRECIO_BASE_ORIG")]
            public string I_PRECIO_BASE_ORIG { get; set; }
            [XmlElement(ElementName = "I_PRECIO_VTA")]
            public string I_PRECIO_VTA { get; set; }
            [XmlElement(ElementName = "I_BAUTIZADO")]
            public string I_BAUTIZADO { get; set; }
            [XmlElement(ElementName = "I_PRECIO_MANUAL")]
            public string I_PRECIO_MANUAL { get; set; }
            [XmlElement(ElementName = "C_FAMILIA")]
            public string C_FAMILIA { get; set; }
            [XmlElement(ElementName = "M_FACTOR_VTA_ESP")]
            public string M_FACTOR_VTA_ESP { get; set; }
            [XmlElement(ElementName = "M_VENDE_POR_PESO")]
            public string M_VENDE_POR_PESO { get; set; }
            [XmlElement(ElementName = "Q_BULTOS_GRAMOS")]
            public string Q_BULTOS_GRAMOS { get; set; }
            [XmlElement(ElementName = "Q_FACTOR_PZAS")]
            public string Q_FACTOR_PZAS { get; set; }
            [XmlElement(ElementName = "I_PRECIO_UNIT_ART")]
            public string I_PRECIO_UNIT_ART { get; set; }
            [XmlElement(ElementName = "I_PRECIO_UNIT_ART_FINAL")]
            public string I_PRECIO_UNIT_ART_FINAL { get; set; }
            [XmlElement(ElementName = "K_IVA")]
            public string K_IVA { get; set; }
            [XmlElement(ElementName = "I_TOTAL_ART")]
            public string I_TOTAL_ART { get; set; }
            [XmlElement(ElementName = "Q_PESO_TOTAL_ARTICULO")]
            public string Q_PESO_TOTAL_ARTICULO { get; set; }
            [XmlElement(ElementName = "C_ENVASE")]
            public string C_ENVASE { get; set; }
            [XmlElement(ElementName = "I_TOTAL_NETO_ENVASES")]
            public string I_TOTAL_NETO_ENVASES { get; set; }
            [XmlElement(ElementName = "Q_UNID_GRAMOS_PEDIDO")]
            public string Q_UNID_GRAMOS_PEDIDO { get; set; }
            [XmlElement(ElementName = "Q_UNID_GRAMOS_CUMPLIDO")]
            public string Q_UNID_GRAMOS_CUMPLIDO { get; set; }
            [XmlElement(ElementName = "Q_UNID_GRAMOS_SALDO")]
            public string Q_UNID_GRAMOS_SALDO { get; set; }
            [XmlElement(ElementName = "C_DOC_LETRA_FACT")]
            public string C_DOC_LETRA_FACT { get; set; }
            [XmlElement(ElementName = "U_DOC_PREFIJO_FACT")]
            public string U_DOC_PREFIJO_FACT { get; set; }
            [XmlElement(ElementName = "U_DOC_SUFIJO_FACT")]
            public string U_DOC_SUFIJO_FACT { get; set; }
            [XmlElement(ElementName = "F_ALTA_SIST")]
            public string F_ALTA_SIST { get; set; }
            [XmlElement(ElementName = "K_DESC_APLIC_A_PRECIO_VTA")]
            public string K_DESC_APLIC_A_PRECIO_VTA { get; set; }
            [XmlElement(ElementName = "M_MOBILE")]
            public string M_MOBILE { get; set; }
            [XmlElement(ElementName = "M_CAJA_CERRADA")]
            public string M_CAJA_CERRADA { get; set; }
            [XmlElement(ElementName = "M_MODIFICA_LINEA")]
            public string M_MODIFICA_LINEA { get; set; }
            [XmlElement(ElementName = "Q_UNID_GRAMOS_PREPARADO")]
            public string Q_UNID_GRAMOS_PREPARADO { get; set; }
            [XmlElement(ElementName = "M_ARTICULOS_EXCLUYENTE")]
            public string M_ARTICULOS_EXCLUYENTE { get; set; }
            [XmlElement(ElementName = "C_ARTICULO_REEMPLAZA")]
            public string C_ARTICULO_REEMPLAZA { get; set; }
            [XmlElement(ElementName = "TRANSID")]
            public string TRANSID { get; set; }
        }

        [XmlRoot(ElementName = "Detalle")]
        public class Detalle
        {
            [XmlElement(ElementName = "PostD")]
            public List<PostD> PostD { get; set; }
        }

    }
}
