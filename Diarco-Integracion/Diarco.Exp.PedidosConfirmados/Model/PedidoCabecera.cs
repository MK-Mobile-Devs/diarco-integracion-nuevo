﻿using System.Xml.Serialization;

namespace Diarco.Exp.PedidosConfirmados.Model
{
    [XmlRoot(ElementName = "Post")]
    public class Post
    {
        [XmlElement(ElementName = "TRANSID")]
        public string TRANSID { get; set; }
        [XmlElement(ElementName = "USERID")]
        public string USERID { get; set; }
        [XmlElement(ElementName = "FECHA_SYNCHRO")]
        public string FECHA_SYNCHRO { get; set; }
        [XmlElement(ElementName = "FECHA_FACTURAR")]
        public string FECHA_FACTURAR { get; set; }
        [XmlElement(ElementName = "FECHA")]
        public string FECHA { get; set; }
        [XmlElement(ElementName = "CLIENTE")]
        public string CLIENTE { get; set; }
        [XmlElement(ElementName = "RAZONSOCIAL")]
        public string RAZONSOCIAL { get; set; }
        [XmlElement(ElementName = "VENDEDOR")]
        public string VENDEDOR { get; set; }
        [XmlElement(ElementName = "NOMBREVENDEDOR")]
        public string NOMBREVENDEDOR { get; set; }
        [XmlElement(ElementName = "SUCURSAL")]
        public string SUCURSAL { get; set; }
        [XmlElement(ElementName = "ZONA")]
        public string ZONA { get; set; }
        [XmlElement(ElementName = "ESTADO")]
        public string ESTADO { get; set; }
        [XmlElement(ElementName = "IMPORTE_LISTA")]
        public string IMPORTE_LISTA { get; set; }
        [XmlElement(ElementName = "IMPORTE_PROPUESTO")]
        public string IMPORTE_PROPUESTO { get; set; }
        [XmlElement(ElementName = "IMPORTE_FINAL")]
        public string IMPORTE_FINAL { get; set; }
        [XmlElement(ElementName = "UTILIDAD_LISTA")]
        public string UTILIDAD_LISTA { get; set; }
        [XmlElement(ElementName = "UTILIDAD_PROPUESTA")]
        public string UTILIDAD_PROPUESTA { get; set; }
        [XmlElement(ElementName = "UTILIDAD_FINAL")]
        public string UTILIDAD_FINAL { get; set; }
        [XmlElement(ElementName = "RENTABILIDAD_LISTA")]
        public string RENTABILIDAD_LISTA { get; set; }
        [XmlElement(ElementName = "RENTABILIDAD_PROPUESTA")]
        public string RENTABILIDAD_PROPUESTA { get; set; }
        [XmlElement(ElementName = "RENTABILIDAD_FINAL")]
        public string RENTABILIDAD_FINAL { get; set; }
        [XmlElement(ElementName = "TIPO_PEDIDO")]
        public string TIPO_PEDIDO { get; set; }
        [XmlElement(ElementName = "CUIT")]
        public string CUIT { get; set; }
        [XmlElement(ElementName = "CONDIVA")]
        public string CONDIVA { get; set; }
        [XmlElement(ElementName = "CPOSTALCLIENTE")]
        public string CPOSTALCLIENTE { get; set; }
        [XmlElement(ElementName = "CPROVINCIACLIENTE")]
        public string CPROVINCIACLIENTE { get; set; }
        [XmlElement(ElementName = "NDIRECCIONCLIENTE")]
        public string NDIRECCIONCLIENTE { get; set; }
        [XmlElement(ElementName = "NLOCALIDADCLIENTE")]
        public string NLOCALIDADCLIENTE { get; set; }
        [XmlElement(ElementName = "FORMAPAGO")]
        public string FORMAPAGO { get; set; }
        [XmlElement(ElementName = "OBSERVACION")]
        public string OBSERVACION { get; set; }
        [XmlElement(ElementName = "FECHAENTREGADISPLAY")]
        public string FECHAENTREGADISPLAY { get; set; }
        [XmlElement(ElementName = "FECHAENTREGA")]
        public string FECHAENTREGA { get; set; }
        [XmlElement(ElementName = "BULTOS")]
        public string BULTOS { get; set; }
        [XmlElement(ElementName = "CTARJETA")]
        public string CTARJETA { get; set; }
        [XmlElement(ElementName = "TARJETA")]
        public string TARJETA { get; set; }
        [XmlElement(ElementName = "CUOTAS")]
        public string CUOTAS { get; set; }
        [XmlElement(ElementName = "COSTO_TOTAL")]
        public string COSTO_TOTAL { get; set; }
        [XmlElement(ElementName = "EXENTOIVA")]
        public string EXENTOIVA { get; set; }
        [XmlElement(ElementName = "IBCLIENTE")]
        public string IBCLIENTE { get; set; }
        [XmlElement(ElementName = "FACTURADO")]
        public string FACTURADO { get; set; }
        [XmlElement(ElementName = "ENTREGADO")]
        public string ENTREGADO { get; set; }
        [XmlElement(ElementName = "AUTORIZO_CREDITOS")]
        public string AUTORIZO_CREDITOS { get; set; }
        [XmlElement(ElementName = "AUTORIZO_ADMVTAS")]
        public string AUTORIZO_ADMVTAS { get; set; }
        [XmlElement(ElementName = "MAIL")]
        public string MAIL { get; set; }
        [XmlElement(ElementName = "TIPO_PEDIDO_DESC")]
        public string TIPO_PEDIDO_DESC { get; set; }
        [XmlElement(ElementName = "FORMA_PAGO_DESC")]
        public string FORMA_PAGO_DESC { get; set; }
        [XmlElement(ElementName = "SEMAFORO")]
        public string SEMAFORO { get; set; }
        [XmlElement(ElementName = "ESCOMISIONISTA")]
        public string ESCOMISIONISTA { get; set; }
        [XmlElement(ElementName = "COMISIONISTAID")]
        public string COMISIONISTAID { get; set; }
        [XmlElement(ElementName = "MAIL2")]
        public string MAIL2 { get; set; }
        [XmlElement(ElementName = "MailSucursal")]
        public string MailSucursal { get; set; }
        [XmlElement(ElementName = "MAIL3")]
        public string MAIL3 { get; set; }
        [XmlElement(ElementName = "BAUTIZADOS")]
        public string BAUTIZADOS { get; set; }
        [XmlElement(ElementName = "U_DOC_SUFIJO")]
        public string U_DOC_SUFIJO { get; set; }
        [XmlElement(ElementName = "K_RECAR_ART_OFER")]
        public string K_RECAR_ART_OFER { get; set; }
        [XmlElement(ElementName = "K_RECAR_ART_NO_OFER")]
        public string K_RECAR_ART_NO_OFER { get; set; }
    }

    [XmlRoot(ElementName = "Cabecera")]
    public class Cabecera
    {
        [XmlElement(ElementName = "Post")]
        public Post Post { get; set; }
    }

}
