﻿namespace Diarco.Exp.PedidosConfirmados.Model
{
    public class XmlPedido
    {
        public string CabeceraXml { get; set; }
        public string DetalleXml { get; set; }
    }
}
