﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml.Serialization;
using Diarco.Data.UnitOfWork;
using Diarco.Exp.PedidosConfirmados.Model;
using MobileAssistant.Data.Helpers;
using MobileAssistant.Integracion;
using MobileAssistant.Integracion.Helpers;

namespace Diarco.Exp.PedidosConfirmados
{
    public class Job : BaseJob 
    {

        private static string GetConnectionString(ModuleParameters parameters)
        {
            return ConnectionStringHelper.CreateSqlConnection(
                parameters.RegistryParameters.DbServerName, parameters.RegistryParameters.DbName,
                parameters.RegistryParameters.DbUserId, parameters.RegistryParameters.DbPassword,
                @"res://*/DiarcoModel.csdl|res://*/DiarcoModel.ssdl|res://*/DiarcoModel.msl");
        }

        protected override void Run(ModuleParameters parameters)
        {
            var connectionString = GetConnectionString(parameters);
            var unitOfWork = new UnitOfWork(new Data.MADiarcoEntities(connectionString));
            ExecuteJob(unitOfWork, parameters.TaskName);
        }

        private void ExecuteJob(IUnitOfWork unitOfWork, string taskName)
        {
            var pedidos = unitOfWork.PedidosRepository.ObtenerPedidosAConfirmar().ToList();

            foreach (var pedido in pedidos)
            {
                LogId = CrearLogCabecera(taskName, pedido.TRANSID, pedido.UserID);
                var pedidoXml = unitOfWork.PedidosRepository.ExportarPedidoConfirmado(pedido.TRANSID, pedido.UserID, LogId);

                //Transformo en objeto el XML de la cabecera, lo hago a este alcance porque necesito datos para
                //armar el mail que informa que el cliente no existe en caso de venir la variable @ClienteInexistente = True
                var serializer = new XmlSerializer(typeof(Cabecera));
                using (TextReader cabeceraReader = new StringReader(pedidoXml.CabeceraXml))
                {
                    var result = (Cabecera)serializer.Deserialize(cabeceraReader);

                    //Pregunto si el cliente existe en la base de ellos
                    if (pedidoXml.ClienteInexistente != null && (bool)!pedidoXml.ClienteInexistente)
                    {
                        //Armo la cabecera del mail para el pedido
                        var mailMessage = "CLIENTE".PadRight(20) + result.Post.RAZONSOCIAL + "\n";
                        mailMessage += "NUM CLIENTE".PadRight(20) + result.Post.CLIENTE + "\n";
                        mailMessage += "CUIT".PadRight(20) + result.Post.CUIT + "\n";
                        mailMessage += "TIPO PEDIDO".PadRight(20) + result.Post.TIPO_PEDIDO_DESC + "\n";
                        mailMessage += "DIRECCION".PadRight(20) + result.Post.NDIRECCIONCLIENTE + " " + result.Post.NLOCALIDADCLIENTE + "\n";
                        mailMessage += "FORMA PAGO".PadRight(20) + result.Post.FORMA_PAGO_DESC + "\n";
                        mailMessage += "FECHA ENTREGA".PadRight(20) + result.Post.FECHAENTREGA + "\n";
                        mailMessage += "OBSERVACIONES".PadRight(20) + result.Post.OBSERVACION + "\n";
                        mailMessage += "TOTAL SIN IMP".PadRight(20) + result.Post.IMPORTE_FINAL + "\n";
                        mailMessage += "IVA".PadRight(20) + result.Post.CONDIVA + "\n";
                        mailMessage += "TOTAL CON IMP".PadRight(20) + 0 + "\n";
                        mailMessage += "TOTAL DE BULTOS".PadRight(20) + result.Post.BULTOS + "\n";
                        mailMessage += "CORREO ELECTRONICO".PadRight(20) + result.Post.MAIL + "\n";
                        mailMessage += "\n";
                        mailMessage += "Artículo".PadRight(8) + " " + "Descripción".PadRight(22) + " " +
                                       "Bultos/Pzs".PadRight(10) + " " + "UXB/Kg".PadRight(7) + " " +
                                       "Prec Unit".PadRight(14) + " " + "Tot Línea".PadRight(14) + "\n" +
                                       "".PadRight(8, '=') + " " + "".PadRight(22, '=') + " " + "".PadRight(10, '=') + " " +
                                       "".PadRight(7, '=') + " " + "".PadRight(14, '=') + " " + "".PadRight(14, '=') + "\n";


                        //Transformo en objeto el XML del detalle del pedido
                        var serializerDet = new XmlSerializer(typeof(PedidoDetalle.Detalle));
                        using (TextReader detalleReader = new StringReader(pedidoXml.DetalleXml))
                        {
                            var det = (PedidoDetalle.Detalle)serializerDet.Deserialize(detalleReader);

                            foreach (var i in det.PostD)
                            {
                                mailMessage += i.C_ARTICULO.PadRight(8) + " " + i.N_ARTICULO.Substring(0,20).PadRight(22) + " " +
                                               i.Q_BULTOS_GRAMOS.PadRight(10) + " " + 
                                               decimal.Round(decimal.Parse(i.Q_FACTOR_PZAS),2).ToString(CultureInfo.InvariantCulture).PadRight(7) + " " +
                                               decimal.Round(decimal.Parse(i.I_PRECIO_MANUAL), 2).ToString(CultureInfo.InvariantCulture).PadRight(14) + " " +
                                               decimal.Round(decimal.Parse(i.I_TOTAL_ART), 2).ToString(CultureInfo.InvariantCulture).PadRight(14);
                                           
                                if (i.M_ARTICULOS_EXCLUYENTE != "N")
                                {
                                    mailMessage += " rplz: " + i.C_ARTICULO_REEMPLAZA.PadRight(8);
                                }

                                mailMessage += "\n";
                            }
                        }

                        //Envio todos los mails
                        SendEmail(unitOfWork, "pr@mkmobile.com.ar", "Su Pedido número: " + pedido.TRANSID, mailMessage,
                            pedido.TRANSID);
                        //SendEmail(unitOfWork, result.Post.MAIL, "Su Pedido número: " + pedido.TRANSID, mailMessage,
                        //    pedido.TRANSID);
                        //SendEmail(unitOfWork, result.Post.MAIL3, "Su Pedido número: " + pedido.TRANSID, mailMessage,
                        //    pedido.TRANSID);
                        //SendEmail(unitOfWork, result.Post.MAIL3, "Su Pedido número: " + pedido.TRANSID, mailMessage,
                        //    pedido.TRANSID);
                        //SendEmail(unitOfWork, result.Post.MailSucursal, "Su Pedido número: " + pedido.TRANSID, mailMessage,
                        //    pedido.TRANSID);

                    }
                    else
                    {
                        //En caso que el cliente no exista en la base de Diarco, informo a sistemas
                        SendEmail(unitOfWork, "pr@mkmobile.com.ar",
                            "Condición de IVA del cliente " + result.Post.CLIENTE + " inexistente.",
                            "Pedido " + pedido.TRANSID + " de cliente: " + result.Post.CLIENTE +
                            " sin condición de IVA. Verificar si existe el cliente o se ha dado de baja",
                            pedido.TRANSID);
                    }
                }
            }
        }

        private void SendEmail(IUnitOfWork unitOfWork, string address, string subject, string body, string transId)
        {
            if (unitOfWork.PedidosRepository.MailYaEnviado(transId) == null)
            {
                var sb = new StringBuilder();
                sb.Append(body);

                var parametros = MAUnitOfWork.ParametrosGeneralesRepository.GetAll().ToList();
                var smtpClient = parametros.First(p => p.VarName == "SMTPServer").VarValue;
                var smtpUser = parametros.First(p => p.VarName == "SMTPUser").VarValue;
                var smtpPassword = parametros.First(p => p.VarName == "SMTPPass").VarValue;

                try
                {
                    var mail = new MailMessage(smtpUser, address, subject, body);
                    var smtpServer = new SmtpClient(smtpClient)
                    {
                        Host = "10.0.0.11",
                        Port = 587,
                        Credentials = new NetworkCredential(smtpUser, smtpPassword)
                    };
                    smtpServer.Send(mail);
                    LogEnBaseDeDatos($"Email enviado con éxito a {address}\nSubject: {subject}\nbody:{body}");
                    unitOfWork.PedidosRepository.MdNotifierInsertar(" ", "Diarco Test - Drco_Exp_PedidosConfirmados", "Tarea:Diarco Test - Drco_Exp_PedidosConfirmados", "Ok", " ", "Ok", "N", DateTime.Now, transId);
                }
                catch (Exception ex)
                {
                    LogErrorEnBaseDeDatos($"Error al enviar email\n {ex}");
                }
            }
        }
    }
}
