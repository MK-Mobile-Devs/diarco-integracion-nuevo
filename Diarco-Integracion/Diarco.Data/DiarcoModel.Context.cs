﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diarco.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class MADiarcoEntities : DbContext
    {
        public MADiarcoEntities()
            : base("name=MADiarcoEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
    
        public virtual ObjectResult<Diarco_MA_ObtenerPedidosAConfirmar_Result> Diarco_MA_ObtenerPedidosAConfirmar()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Diarco_MA_ObtenerPedidosAConfirmar_Result>("Diarco_MA_ObtenerPedidosAConfirmar");
        }
    
        public virtual ObjectResult<Diarco_MA_ExportarPedidoConfirmado_Result> Diarco_MA_ExportarPedidoConfirmado(string transId, string userId, Nullable<int> logId)
        {
            var transIdParameter = transId != null ?
                new ObjectParameter("TransId", transId) :
                new ObjectParameter("TransId", typeof(string));
    
            var userIdParameter = userId != null ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(string));
    
            var logIdParameter = logId.HasValue ?
                new ObjectParameter("LogId", logId) :
                new ObjectParameter("LogId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Diarco_MA_ExportarPedidoConfirmado_Result>("Diarco_MA_ExportarPedidoConfirmado", transIdParameter, userIdParameter, logIdParameter);
        }
    
        public virtual int Diarco_MA_mDNotifierInsertar(string userID, string subject, string message, string additional, string notifModule, string alertType, string status, Nullable<System.DateTime> createDate, string transId)
        {
            var userIDParameter = userID != null ?
                new ObjectParameter("UserID", userID) :
                new ObjectParameter("UserID", typeof(string));
    
            var subjectParameter = subject != null ?
                new ObjectParameter("Subject", subject) :
                new ObjectParameter("Subject", typeof(string));
    
            var messageParameter = message != null ?
                new ObjectParameter("Message", message) :
                new ObjectParameter("Message", typeof(string));
    
            var additionalParameter = additional != null ?
                new ObjectParameter("Additional", additional) :
                new ObjectParameter("Additional", typeof(string));
    
            var notifModuleParameter = notifModule != null ?
                new ObjectParameter("NotifModule", notifModule) :
                new ObjectParameter("NotifModule", typeof(string));
    
            var alertTypeParameter = alertType != null ?
                new ObjectParameter("AlertType", alertType) :
                new ObjectParameter("AlertType", typeof(string));
    
            var statusParameter = status != null ?
                new ObjectParameter("Status", status) :
                new ObjectParameter("Status", typeof(string));
    
            var createDateParameter = createDate.HasValue ?
                new ObjectParameter("CreateDate", createDate) :
                new ObjectParameter("CreateDate", typeof(System.DateTime));
    
            var transIdParameter = transId != null ?
                new ObjectParameter("TransId", transId) :
                new ObjectParameter("TransId", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Diarco_MA_mDNotifierInsertar", userIDParameter, subjectParameter, messageParameter, additionalParameter, notifModuleParameter, alertTypeParameter, statusParameter, createDateParameter, transIdParameter);
        }
    
        public virtual ObjectResult<string> Diarco_MA_MailYaEnviado(string transId)
        {
            var transIdParameter = transId != null ?
                new ObjectParameter("TransId", transId) :
                new ObjectParameter("TransId", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("Diarco_MA_MailYaEnviado", transIdParameter);
        }
    }
}
