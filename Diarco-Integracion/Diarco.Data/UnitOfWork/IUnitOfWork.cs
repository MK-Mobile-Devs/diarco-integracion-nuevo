﻿using System;
using Diarco.Data.Repository.Pedidos;

namespace Diarco.Data.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        MADiarcoEntities Context { get; }
        PedidosRepository PedidosRepository{ get; }
        void Save();
    }
}
