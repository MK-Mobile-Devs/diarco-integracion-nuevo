﻿

using Diarco.Data.Repository.Pedidos;

namespace Diarco.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
   
        #region private members

        private PedidosRepository _pedidosRepository;
        
        #endregion

        #region public members

        public UnitOfWork(MADiarcoEntities context)
        {
            Context = context;
            Context.Database.CommandTimeout = 180;
        }

        public MADiarcoEntities Context { get; }

        public PedidosRepository PedidosRepository
            => _pedidosRepository ?? (_pedidosRepository = new PedidosRepository(Context));

        public void Save()
        {
            Context.SaveChanges();
        }

        #endregion

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
