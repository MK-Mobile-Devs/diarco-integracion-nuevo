//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Diarco.Data
{
    using System;
    
    public partial class Diarco_MA_ExportarPedidoConfirmado_Result
    {
        public string CabeceraXml { get; set; }
        public string DetalleXml { get; set; }
        public Nullable<bool> ClienteInexistente { get; set; }
    }
}
