﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Diarco.Data.Repository.Pedidos
{
    public class PedidosRepository : BaseRepository
    {
        public Diarco_MA_ExportarPedidoConfirmado_Result ExportarPedidoConfirmado(string transId, string userId, int logId)
        {
            return Contexto.Diarco_MA_ExportarPedidoConfirmado(transId, userId, logId).FirstOrDefault();
        }

        public IEnumerable<Diarco_MA_ObtenerPedidosAConfirmar_Result> ObtenerPedidosAConfirmar()
        {
            return Contexto.Diarco_MA_ObtenerPedidosAConfirmar();
        }

        public string MailYaEnviado(string transId)
        {
            return Contexto.Diarco_MA_MailYaEnviado(transId).FirstOrDefault();
        }

        public void MdNotifierInsertar(string userId, string subject, string message, string additional, string notifModule, string alertType, 
            string status, DateTime? createDate, string transId)
        {
            Contexto.Diarco_MA_mDNotifierInsertar(userId, subject, message, additional, notifModule, alertType, status,
                createDate, transId);
        }

        public PedidosRepository(MADiarcoEntities contexto) : base(contexto)
        {

        }
    }
}
