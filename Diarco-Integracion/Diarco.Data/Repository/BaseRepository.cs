﻿namespace Diarco.Data.Repository
{
    public class BaseRepository
    {
        public MADiarcoEntities Contexto { get;}

        public BaseRepository(MADiarcoEntities contexto)
        {
            Contexto = contexto;
        }
        
    }
}
